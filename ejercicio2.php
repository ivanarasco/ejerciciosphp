<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2 PHP</title>
    </head>
</head>
<body>
    <h2> Precio de un producto </h2>

<?php

$precioUnidad = 20;
$cantidad = 3;
$iva = 0.21;
$precioSinIva = $precioUnidad * $cantidad ;
$precioConIva = $iva * ($precioUnidad * $cantidad) + $precioSinIva;

echo "El precio de una unidad es: $precioUnidad ". "<br>";
echo "El precio total sin iva: $precioSinIva €". "<br>";
echo "El precio total con iva: $precioConIva €" ;


?>

</body>
</html>
