<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <title>Jugadores de Baloncesto</title>
</head>
<body>
<?php

$array1 = array (
    'Jugador 1' => 'Iván Arasco',
    'Jugador 2' => 'David Martinez',
    'Jugador 3' => 'María Tejel',
    'Jugador 4' => 'Gervasio Asd',
    'Jugador 5' => 'Hermenegildo Gracia'
);

//foreach clave->elemento

echo "Recorrido mediante bucle foreach con posicion->elemento <br>";

echo "<br>";

foreach ($array1 as $position=>$element){
    echo $position . ": " . $element . '<br>';
}

?>
</body>
</html>


