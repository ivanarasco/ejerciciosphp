<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 3 PHP</title>
    </head>
</head>
<body>
   <h2> Días de la Semana </h2>

<?php

$array1 = array("Lunes","Martes", "Miércoles", "Jueves", "Viernes"); // array declarado en una línea.

echo "<hr> Recorrido de array con elementos insertados en una línea. <br> <br>";
echo "<ul>";
foreach ($array1 as $element){

        echo "<li>";
            echo $element . '<br>';
        echo "</li>";

}
 echo "</ul>";
var_dump($array1);

$array2[] = "Lunes"; // Insertamos elementos de 1 en 1 sin necesidad de declarar el array.
$array2[] = "Martes";
$array2[] = "Miércoles";
$array2[] = "Jueves";
$array2[] = "Viernes";

echo "<hr> Recorrido de array con elementos insertados de 1 en 1. <br> <br>";
echo "<table border=1px>";
foreach ($array2 as $element){
    echo "<tr>";
        echo "<td>";
            echo $element . '<br>';
        echo "</td>";
    echo "</tr>";
}
echo "</table>";
var_dump($array2);
?>

</body>
</html>
