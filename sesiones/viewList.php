<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Sesiones</title>
</head>
<body>
    <h2> Lista de Deseos de <?php echo isset($_SESSION['user']) ? $_SESSION['user'] : '' ?>
    </h2>
    <p><a href="?method=cerrarSesion"> Cerrar sesión </a></p>

    <form method="post" action="?method=new">
        <label> Deseo: </label>
        <input type="text" name="deseo">
        <br>
        <input type="submit" value="enviar">

    </form>

    <hr>
    <h3> Lista de Deseos </h3>

    <ul>
        <?php foreach ($deseos as $clave => $deseo): ?>
            <li> <?php echo $deseo ?>
                <a href="?method=delete&key=<?php echo $clave ?>"> Borrar </a>
            </li>

        <?php endforeach ?>

    </ul>
    <a href="?method=deleteList"> Borrar todos la lista</a>
</body>
</html>

