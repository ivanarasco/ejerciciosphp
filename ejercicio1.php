<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1 PHP</title>
    </head>
</head>
<body>
    <h2> Mis Datos Personales </h2>

<?php

$nombre = "Ivan";
$apellido1 = "Arasco";
$apellido2 = "Millán";
$edad = 21;

echo "Mi nombre es: " . $nombre . '.' . '<br>' ;
print "Mis apellidos son: " . $apellido1 . " $apellido2" . '.' .'<br>';
echo "Tengo $edad años" . '.' .'<br>';
print 'La variable que almacena la edad se llama $edad' . '.' .'<br>';
echo 'Y es indiferente poner print que echo, $edad va a seguir siendo: ' . $edad;
echo "Mi nombre completo es $nombre , $apellido1 , $apellido2 , y tengo: $edad años" .'.';

 ?>
</body>
</html>
