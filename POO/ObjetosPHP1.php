<?php

class ObjetosPHP1{

    private $atributo;

    function __construct($param){
        echo "Construyendo mi aplicación<br>";
        $this->atributo = $param; // Que llame al método de index() por cada objeto que se cree.
        // this.nombre = "atributo1";
    }

    public function index(){
        echo "En mi método index <br>";
        if (isset($_COOKIE['nombre'])){
            $nombre = $_COOKIE['nombre'];
        }
        require('viewLogin.php');
    }

    public function hello(){

        echo "Hello World!";
    }

    public function home(){

       if (isset($_REQUEST['nombre']) && !empty ($_REQUEST['nombre'])){ // nombre del nuevo usuario
        $nombre = $_REQUEST['nombre']; // lo uso
        if (isset($_REQUEST['remember'])){
             setcookie('nombre', $nombre);
        }else{
            setcookie('nombre', $nombre, 1); // la borro
        }

        }

        require('viewHome.php');

    }
}
